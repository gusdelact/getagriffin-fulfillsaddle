package fulfillsaddle.stub;

/* */
public interface FulfillSaddleStub {
	String requisitionFromInventory(int deptNo) throws Exception;
	void cancelRequisition(String saddleId) throws Exception;
	void addToInventory(String saddleId) throws Exception;
	void haltRequisitions() throws Exception;
	void enableRequisitions() throws Exception;
	String ping() throws Exception;
}
