package fulfillsaddle.stub;

import java.util.SortedSet;
import java.util.TreeSet;
import java.util.Map;
import java.util.HashMap;

public class FulfillSaddleFactoryMock implements FulfillSaddleFactory {

	private SortedSet<String> availSaddles = new TreeSet<String>();

	public FulfillSaddleFactoryMock() {

		availSaddles.add("abc");
		availSaddles.add("def");
	}

	public FulfillSaddleStub createFulfillSaddle() {
		return new FulfillSaddleStub() {
			/* Mock implementations */

			private boolean reqsEnabled = true;

			public String requisitionFromInventory(int deptNo) throws Exception {
				if (! this.reqsEnabled) throw new Exception("No saddles available");
				if (availSaddles.size() == 0) throw new Exception("No saddles available");
				String saddleId = availSaddles.first();
				availSaddles.remove(saddleId);
				return saddleId;
			}

			public void addToInventory(String saddleId) throws Exception {
				try { availSaddles.add(saddleId); }
				catch (Exception ex) { throw new Exception("Unable to add saddle"); }
			}

			public void cancelRequisition(String saddleId) throws Exception {
				try { availSaddles.add(saddleId); }
				catch (Exception ex) { throw new Exception("Unable to add saddle"); }
			}

			public void haltRequisitions() throws Exception {
				this.reqsEnabled = false;
			}

			public void enableRequisitions() throws Exception {
				this.reqsEnabled = true;
			}

			public String ping() throws Exception {
				return "ping";
			}
		};
	}
}
