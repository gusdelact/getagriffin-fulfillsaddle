package fulfillsaddle.main;

import static spark.Spark.*;
import com.mysql.jdbc.Driver;
//import oracle.jdbc.driver.OracleDriver;
import com.google.gson.Gson;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FulfillSaddle {

	private String jdbcPort;
	private String jdbcUrl;
	private String jdbcUser;
	private String jdbcPass;
	private Logger logger = LoggerFactory.getLogger(FulfillSaddle.class);

	/** The main method starts this microservice. */
	public static void main(String[] args) {

		FulfillSaddle fulfillSaddle = new FulfillSaddle();
		Logger logger = fulfillSaddle.logger;

		get("/ping", (req, res) -> {
			logger.info("Received ping");
			return "ping";
		});

		get("/RequisitionFromInventory", (req, res) -> {

			try {
				logger.info("Received request RequisitionFromInventory...");
				String deptNoStr = req.queryParams("DeptNo");
				if (deptNoStr == null) {
					throw new Exception("Missing DeptNo parameter");
				}
				int deptNo = 0;
				try {
					deptNo = Integer.parseInt(deptNoStr);
				} catch (NumberFormatException ex) {
					throw new Exception("DeptNo must be an integer");
				}

				String saddleId = fulfillSaddle.requisitionFromInventory(deptNo);

				RequisitionResponse response = new RequisitionResponse(saddleId);
				if (response == null) throw new RuntimeException("From new RequisitionResponse: returned null");

				Gson gson = new Gson();
	 			String json = json = gson.toJson(response);

				return json.toString();

			} catch (RuntimeException re) {
				logger.debug(re.getMessage(), re);
				res.status(500);
				String msg = re.getMessage();
				if (msg == null) return "No message";
				return msg;
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				res.status(400);
				String msg = ex.getMessage();
				if (msg == null) return "No message";
				return msg;
			} catch (Throwable t) {
				logger.debug(t.getMessage(), t);
				res.status(500);
				String msg = t.getMessage();
				if (msg == null) return "No message";
				return msg;
			}
		});

		get("/AddToInventory", (req, res) -> {

			try {
				logger.info("Received request AddToInventory...");
				String saddleId = req.queryParams("SaddleId");
				if (saddleId == null) {
					throw new Exception("Missing SaddleId parameter");
				}

				fulfillSaddle.addToInventory(saddleId);
				return "added to inventory";

			} catch (RuntimeException re) {
				logger.debug(re.getMessage(), re);
				res.status(500);
				String msg = re.getMessage();
				if (msg == null) return "No message";
				return msg;
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				res.status(400);
				String msg = ex.getMessage();
				if (msg == null) return "No message";
				return msg;
			} catch (Throwable t) {
				logger.debug(t.getMessage(), t);
				res.status(500);
				String msg = t.getMessage();
				if (msg == null) return "No message";
				return msg;
			}
		});

		get("/CancelRequisition", (req, res) -> {

			try {
				logger.info("Received request CancelRequisition...");
				String saddleId = req.queryParams("SaddleId");
				if (saddleId == null) {
					throw new Exception("Missing SaddleId parameter");
				}

				fulfillSaddle.cancelRequisition(saddleId);
				return "saddle requisition cancelled";

			} catch (RuntimeException re) {
				logger.debug(re.getMessage(), re);
				res.status(500);
				String msg = re.getMessage();
				if (msg == null) return "No message";
				return msg;
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				res.status(400);
				String msg = ex.getMessage();
				if (msg == null) return "No message";
				return msg;
			} catch (Throwable t) {
				logger.debug(t.getMessage(), t);
				res.status(500);
				String msg = t.getMessage();
				if (msg == null) return "No message";
				return msg;
			}
		});

		get("/ping", (req, res) -> {
			try {
				logger.info("Received ping...");
				return "ping";
			} catch (RuntimeException re) {
				logger.debug(re.getMessage(), re);
				res.status(500);
				String msg = re.getMessage();
				if (msg == null) return "No message";
				return msg;
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				res.status(400);
				String msg = ex.getMessage();
				if (msg == null) return "No message";
				return msg;
			} catch (Throwable t) {
				logger.debug(t.getMessage(), t);
				res.status(500);
				String msg = t.getMessage();
				if (msg == null) return "No message";
				return msg;
			}
		});
	}

	static class RequisitionResponse {
		public String saddleId;
		public RequisitionResponse(String saddleId) { this.saddleId = saddleId; }
		public void setSaddleId(String saddleId) { this.saddleId = saddleId; }
		public String getSaddleId() { return this.saddleId; }
	}

	private FulfillSaddle() {

		System.err.println("Initializing FulfillSaddle");

		this.jdbcPort = System.getenv("FULFILL_SADDLE_MYSQL_HOST_PORT");
		this.jdbcUrl = "jdbc:mysql://db:" + this.jdbcPort + "/FulfillSaddle?connectTimeout=0&socketTimeout=0&autoReconnect=true";
		//this.jdbcUrl = "jdbc:mysql://localhost:" + this.jdbcPort + "/FulfillSaddle?connectTimeout=0&socketTimeout=0&autoReconnect=true";
		this.jdbcUser = System.getenv("MYSQL_USER");
		this.jdbcPass = System.getenv("MYSQL_PASSWORD");

		if (this.jdbcPort == null) throw new RuntimeException("Env variable FULFILL_SADDLE_MYSQL_HOST_PORT not set");
		if (this.jdbcUser == null) throw new RuntimeException("Env variable MYSQL_USER not set");
		if (this.jdbcPass == null) throw new RuntimeException("Env variable MYSQL_PASSWORD not set");
	}

	/**
	Attempt to remove a saddle from inventory, and return the ID of the saddle.
	(The ID is actually the saddle's name.) If there are none, throw an exception.
	*/
	private String requisitionFromInventory(int deptNo) throws Exception {

		/* Start a transaction. For scaling, this would use a connection pool, so that
		connections are not shared by different calls to this method - enabling this method
		to be reentrant. */
		Connection conn = DriverManager.getConnection(jdbcUrl, jdbcUser, jdbcPass);
		if (conn == null) throw new Exception("Unable to connect to FulfillSaddle database");
		conn.setAutoCommit(false);

		try {
			/* Find a table row whose "DeptNo" field is empty. */
			Statement stmt = conn.createStatement();
			ResultSet resultSet = stmt.executeQuery(
				"SELECT SaddleId, DeptNo FROM FulfillSaddle " +
				"WHERE (DeptNo IS NULL OR DeptNo = 0) LIMIT 1");

			String saddleId = null;
			for (; resultSet.next();) {
				saddleId = resultSet.getString(1);  // get the SaddleId
			}
			if (saddleId == null) throw new Exception("There are no more Saddles in inventory");

			/* Set the "DeptNo" field to deptNo. */
			Statement stmt2 = conn.createStatement();
			stmt2.executeUpdate("UPDATE FulfillSaddle SET DeptNo = " + deptNo +
				" WHERE SaddleId = '" + saddleId + "'");

			/* Commit the transaction. */
			conn.commit();

			return saddleId;

		} catch (Exception ex) {
			conn.rollback();
			throw ex;
		} finally {
			conn.close();
		}
	}

	/**
	Add or return the specified saddle to inventory.
	*/
	private void addToInventory(String saddleId) throws Exception {

		/* Start a transaction. For scaling, this would use a connection pool, so that
		connections are not shared by different calls to this method - enabling this method
		to be reentrant. */
		Connection conn = DriverManager.getConnection(jdbcUrl, jdbcUser, jdbcPass);
		if (conn == null) throw new Exception("Unable to connect to FulfillSaddle database");
		conn.setAutoCommit(false);

		try {
			/* Check if that saddle is already in the database. */
			Statement stmt1 = conn.createStatement();
			ResultSet resultSet = stmt1.executeQuery(
				"SELECT DeptNo FROM FulfillSaddle where SaddleId = '" + saddleId + "'");
			for (; resultSet.next();) {
				throw new Exception("Database already contains saddle '" + saddleId + "'");
			}

			/* Insert the saddle. */
			Statement stmt2 = conn.createStatement();
			stmt2.executeUpdate("INSERT INTO FulfillSaddle VALUES ('" + saddleId + "', 0)");
			conn.commit();

		} catch (Exception ex) {
			conn.rollback();
			throw ex;
		} finally {
			conn.close();
		}
	}

	private void cancelRequisition(String saddleId) throws Exception {

		/* Start a transaction. For scaling, this would use a connection pool, so that
		connections are not shared by different calls to this method - enabling this method
		to be reentrant. */
		Connection conn = DriverManager.getConnection(jdbcUrl, jdbcUser, jdbcPass);
		if (conn == null) throw new Exception("Unable to connect to FulfillSaddle database");
		conn.setAutoCommit(false);

		try {
			Statement stmt = conn.createStatement();
			stmt.executeUpdate("UPDATE FulfillSaddle SET DeptNo = 0" +
				" WHERE SaddleId = '" + saddleId + "'");

		} catch (Exception ex) {
			conn.rollback();
			throw ex;
		} finally {
			conn.close();
		}
	}
}
